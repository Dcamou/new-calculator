import React, { Component } from 'react';
import Button from './Button';
import { buttons } from '../utiles/TabButton'



class Calculator extends Component {
    constructor(props) {
        super(props);
        this.state = {
        displayValue: '0',
    };
}

handleButtonClick = (buttonValue) => {
    if (buttonValue === '=') {
        try {
            const result = eval(this.state.displayValue);
            this.setState({ displayValue: String(result) });
        } catch (error) {
        this.setState({ displayValue: 'Erreur' });
    }
    } else if (buttonValue === 'C') {
        this.setState({ displayValue: '0' });
    } else {
        this.setState((prevState) => ({
            displayValue: prevState.displayValue === '0' ? buttonValue : prevState.displayValue + buttonValue,
        }));
    }
};

render() {
    return (
            <div className="calculator">
                <div className="display">{this.state.displayValue}</div>
                <div className="buttons">
                {buttons.map((label) => (
                    <Button key={label} label={label} onClick={this.handleButtonClick} />
                ))}
                </div>
            </div>
        );
    }
}

export default Calculator;
